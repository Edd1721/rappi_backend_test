'use strict'

class Matrix {
  Create (size) {
    let matrix = []
    for (let x = 0; x < size; x++) {
      matrix[x] = []
      for (let y = 0; y < size; y++) {
        matrix[x][y] = []
        for (let z = 0; z < size; z++) {
          matrix[x][y][z] = 0
        }
      }
    }

    return matrix
  }

  Update (array, matrix) {
    const value = parseInt(array[4])
    const coords = {
      x: parseInt(array[1]) - 1,
      y: parseInt(array[2]) - 1,
      z: parseInt(array[3]) - 1
    }

    matrix[coords.x][coords.y][coords.z] = value
    return matrix
  }

  Query (arr, matrix) {
    let sum = 0
    const initCoords = {
      x: parseInt(arr[1]) - 1,
      y: parseInt(arr[2]) - 1,
      z: parseInt(arr[3]) - 1
    }
    const finalCoords = {
      x: parseInt(arr[4]) - 1,
      y: parseInt(arr[5]) - 1,
      z: parseInt(arr[6]) - 1
    }

    for (let x = initCoords.x; x <= finalCoords.x; x++) {
      for (let y = initCoords.y; y <= finalCoords.y; y++) {
        for (let z = initCoords.z; z <= finalCoords.z; z++) {
          sum += matrix[x][y][z]
        }
      }
    }

    return sum
  }

  GetInstruction (sentence) {
    let instruction = sentence.toUpperCase()

    if (instruction === 'UPDATE') {
      return 0
    } else if (instruction === 'QUERY') {
      return 1
    } else {
      throw new Error('Instruction is not valid.')
    }
  }
}

module.exports = Matrix
