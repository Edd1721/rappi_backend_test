'use strict'

angular.module('app', [
  'ngRoute',
  'Main'
])
.config(['$locationProvider', '$routeProvider',
  ($locationProvider, $routeProvider) => {
    $routeProvider
    .when('/', {
      template: '<main></main>'
    })
    .otherwise({
      redirectTo: '/'
    })

    $locationProvider.html5Mode(true)
  }])
