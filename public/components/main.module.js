'use strict'

angular.module('Main', ['ngRoute'])
  .component('main', {
    templateUrl: './components/main.template.html',
    controller: ['$http', function ($http) {
      let ct = this
      ct.values = []
      ct.input = ''

      ct.Procesar = () => {
        ct.values = ct.input.split('\n')

        let req = {
          method: 'POST',
          url: '/process',
          data: ct.values
        }

        $http(req)
          .then((res) => {
            ct.resultado = res.data
          })
          .catch((err) => {
            console.log(err.data)
            alert(`No fue posible obtener el resultado. \n ${err.data}`)
          })
      }
    }]
  })
