'use strict'

import express from 'express'
import bodyParser from 'body-parser'
import Debug from 'debug'
import Service from './services/service.js'
import Driver from './services/driver.js'
import Push from './services/push.js'

const app = express()
const port = process.env.PORT || 3000
const debug = new Debug('Refactoring:Server')
const service = new Service()
const driver = new Driver()
const push = new Push()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Método Original (Interpretado del código original de php)
app.post('/postConfirm', (req, res) => {
  var id = req.body.serviceId
  var service = Service.find(id)
  // dd(service)
  if (service != null) {
    if (service.statusId == '6') {
      return Response.json({
        error: '2'
      })
    }

    if (service.driverId == null && servicio.statusId == '1') {
      service = Service.update(id, {
        driverId: req.body.driverId,
        statusId: '2'
        //Up Carro
        //,pwd: md5(req.body.pwd)
      })

      Driver.update(req.body.driverId, {
        available: '0'
      })
      var driverTmp = Driver.find(req.body.driverId)

      service.Service(id, {
        carId: driverTmp.carId
        //Up Carro
        //,pwd: md5(req.body.pwd)
      })
      //Notificar al usuario!!
      var pushMessage = 'Tu servicio ha sido confirmado'
      /*service = servServiceice.find(id)
      var push = Push.make()
      if (service.user.type == '1') {//iPhone
          var pushAns = push.ios(service.user.uuid, pushMessage)
      } else {
          var pushAns = push.android(service.user.uuid, pushMessage)
      }*/

      service = Service.find(id)
      var push = Push.make()

      if (service.user.uuid == '') {
        return Response.json({
          error: '0'
        })
      }

      if (service.user.type == '1') { //Iphone
        var result = push.ios(service.user.uuid, pushMessage, 1, 'honk.wav', 'Open', {
          serviceId: service.id
        })
      } else {
        var result = push.android2(service.user.uuid, pushMessage, 1, 'default', 'Open', {
          serviceId: service.id
        })
      }

      return Response.json({
        error: '0'
      })
    } else {
      return Response.json({
        error: '1'
      })
    }
  } else {
    return Response.json({
      error: '3'
    })
  }
})

// Método refactorizado
app.post('/postConfirm', (req, res) => {
  const messageConfirm = 'Tu servicio ha sido confirmado'
  const id = req.body.serviceId
  const driverId = req.body.driverId
  let serviceRes = service.find(id)
  let validateError = validateService(serviceRes)

  if (validateError <= 3) {
    res.send({
      error: validateError
    })
  }

  try {
    const notification = push.make()
    const carId = driver.find(driverId).carId
    const uuidUser = serviceRes.user.uuid
    const pushServiceId = {
      serviceId: serviceRes.id
    }
    let result = null

    driver.update(driverId, {
      available: 0
    })

    serviceRes = service.update(id, {
      driverId,
      carId,
      statusId: 2
    })

    if (serviceRes.user.type === 1) {
      result = notification.ios(uuidUser, messageConfirm, 1, 'honk.wav', 'Open', pushServiceId)
    } else {
      result = notification.android2(uuidUser, messageConfirm, 1, 'default', 'Open', pushServiceId)
    }

    res.send(result)
  } catch (e) {
    res.send({
      error: 3
    })
  }
})

const validateService = (data) => {
  let error = -1

  if (!data) {
    error = 3
  } else if (data.statusId === 6) {
    error = 2
  } else if (data.driverId && data.statusId !== 1) {
    error = 1
  } else if (!data.user.uuid) {
    error = 0
  }

  return error
}

app.listen(port, () => {
  debug(`Servidor corriento en http://localhost:${port}`)
})
