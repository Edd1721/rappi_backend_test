'use strict'

import express from 'express'
import bodyparser from 'body-parser'
import helmet from 'helmet'
import Debug from 'debug'
import Matrix from './libs/matrix'

const app = express()
const port = process.env.PORT || 3000
const matrix = new Matrix()
const debug = new Debug('CubeSummation:Server:')

app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.set('views', `${__dirname}/public/views`)

app.use(bodyparser.json())
app.use(express.static(`${__dirname}/public`))
app.use(helmet())
app.use(bodyparser.urlencoded({
  extended: false
}))

app.get('/', (req, res) => {
  res.render('index')
})

app.post('/process', (req, res) => {
  let current = 0
  let values = req.body
  let result = []

  let steps = parseInt(values[current++])

  try {
    for (let step = 0; step < steps; step++) {
      let secondIndex = values[current++]
      .split(' ')
      .map((n) => parseInt(n))

      let sizeArray = secondIndex[0]
      let attempts = secondIndex[1]
      let trix = matrix.Create(sizeArray)

      for (let attemp = 0; attemp < attempts; attemp++) {
        let sentence = values[current++]
        let array = sentence.split(' ')
        let query = matrix.GetInstruction(array[0])

        if (query) {
          let ret = matrix.Query(array, trix)
          result.push(ret)
        } else {
          matrix.Update(array, trix)
        }
      }
    }

    res.send(result)
  } catch (e) {
    res.status(500).send(e.message)
  }
})

app.listen(port, () => {
  debug(`Servidor escuchando en el puerto ${port}`)
})
